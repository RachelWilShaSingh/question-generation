# Use Python3
# Set question generator, by Rachel Singh
# https://gitlab.com/RachelWilShaSingh/question-generation

import random
import datetime

nowString = datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y")

output = open( "set-operations-" + nowString + ".txt", "w" )

alphabet = []

A = []
B = []
C = []

U = []

for i in range( 97, 97+25+1 ):
    alphabet.append( chr( i ) )

def PopulateSet():
    tempSet = []
    
    startElement = random.randint( 1, 5 )
    totalElements = random.randint( 4, 7 )
    skipElements = random.randint( 1, 3 )
    
    for i in range( totalElements ):
        choice = alphabet[ startElement + ( skipElements * i ) ]
        
        if ( choice not in tempSet ):
            tempSet.append( choice )
    
    tempSet.sort()
    return tempSet
    
def PrintAndWrite( text, endText ):
    print( text, end=endText )
    output.write( text + endText )
           
def PrintNicely( setname, setlist ):
    PrintAndWrite( setname + " = {", "" )
    first = True
    for el in setlist:
        if ( first == False ):
            PrintAndWrite( ", ", "" )
        PrintAndWrite( el, "" )
        first = False
    PrintAndWrite( "}", "\n" )
    
def GetSetPrime( set1, universe ):
    setprime = set(universe) - set(set1)
    return setprime
    
def GetIntersection( set1, set2 ):
    result = set(set1) & set(set2)
    return result
    
def GetUnion( set1, set2 ):
    result = set(set1) | set(set2)
    return result
    
def GetDifference( set1, set2 ):
    result = set(set1) - set(set2)
    return result
     
A = PopulateSet()
B = PopulateSet()
C = PopulateSet()

for el in A:
    if ( el not in U ):
        U.append( el )
for el in B:
    if ( el not in U ):
        U.append( el )
for el in C:
    if ( el not in U ):
        U.append( el )

U.sort()

PrintNicely( "A", A )
PrintNicely( "B", B )
PrintNicely( "C", C )
PrintNicely( "U", U )
PrintAndWrite( "", "\n" )
PrintNicely( "A'", GetSetPrime( A, U ) )
PrintNicely( "B'", GetSetPrime( B, U ) )
PrintNicely( "C'", GetSetPrime( C, U ) )
PrintAndWrite( "", "\n" )
PrintNicely( "AnB", GetIntersection( A, B ) )
PrintNicely( "AnC", GetIntersection( A, C ) )
PrintNicely( "BnC", GetIntersection( B, C ) )
PrintAndWrite( "", "\n" )
PrintNicely( "AuB", GetUnion( A, B ) )
PrintNicely( "AuC", GetUnion( A, C ) )
PrintNicely( "BuC", GetUnion( B, C ) )
PrintAndWrite( "", "\n" )
PrintNicely( "A-B", GetDifference( A, B ) )
PrintNicely( "A-C", GetDifference( A, C ) )
PrintNicely( "B-A", GetDifference( B, A ) )
PrintNicely( "B-C", GetDifference( B, C ) )
PrintNicely( "C-A", GetDifference( C, A ) )
PrintNicely( "C-B", GetDifference( C, B ) )
PrintAndWrite( "", "\n" )
PrintNicely( "A' n B", GetIntersection( GetSetPrime( A, U ), B ) )
PrintNicely( "A' n C", GetIntersection( GetSetPrime( A, U ), C ) )
PrintNicely( "B' n A", GetIntersection( GetSetPrime( B, U ), B ) )
PrintNicely( "B' n C", GetIntersection( GetSetPrime( B, U ), C ) )
PrintNicely( "C' n A", GetIntersection( GetSetPrime( C, U ), A ) )
PrintNicely( "C' n B", GetIntersection( GetSetPrime( C, U ), B ) )

output.close()
